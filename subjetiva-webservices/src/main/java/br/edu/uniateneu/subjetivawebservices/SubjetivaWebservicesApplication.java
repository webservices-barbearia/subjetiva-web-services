package br.edu.uniateneu.subjetivawebservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SubjetivaWebservicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(SubjetivaWebservicesApplication.class, args);
	}
	
}
