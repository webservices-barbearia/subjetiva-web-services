package br.edu.uniateneu.subjetivawebservices.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.edu.uniateneu.subjetivawebservices.modelo.Agendamentos;
import br.edu.uniateneu.subjetivawebservices.modelo.ResponseModel;
import br.edu.uniateneu.subjetivawebservices.repository.AgendamentosRepository;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.Produces;

@RestController
@RequestMapping(value = "/agendamentos")
public class AgendamentosService {
	@Autowired
	AgendamentosRepository agendamentosRepository;

@Consumes("application/json")
@Produces("application/json")
@RequestMapping(value = "/salvar", method = RequestMethod.POST)
public @ResponseBody Agendamentos salvar(@RequestBody Agendamentos end) {
	Agendamentos entity = this.agendamentosRepository.save(end);
	return entity;

}

@Produces("application/json")
@RequestMapping (value = "/list", method = RequestMethod.GET)
public @ResponseBody List<Agendamentos> listar () {
	List<Agendamentos> entities = this.agendamentosRepository.findAll ();
	return entities;

}

@Consumes("application/json")
@Produces("application/json")
@RequestMapping(value = "/atualizar", method = RequestMethod.PUT)
public @ResponseBody Agendamentos atualizar (@RequestBody Agendamentos end) {
	Agendamentos entity = this.agendamentosRepository.save(end);
	return entity;

}

@Produces("application/json")
@RequestMapping (value = "/deletar/{codigo}", method = RequestMethod.DELETE)
public @ResponseBody ResponseModel excluir(@PathVariable("codigo") Long codigo)
{
	try {
	Agendamentos agendamento = agendamentosRepository.getReferenceById(codigo);
	System.out.println("********"+ Agendamentos.getId());
		
	if (agendamentosRepository.findById(codigo).isEmpty()) {
				return new ResponseModel(500, "Agendamento não encontrado.");
	}	
				agendamentosRepository.delete(agendamento) ;
				return new ResponseModel(200, "Registro excluido com sucesso!");

	} catch (Exception e) {
	return new ResponseModel(500, e.getMessage());
}
}}


