package br.edu.uniateneu.subjetivawebservices.modelo;

import java.sql.Date;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name="tb_agendamentos")

public class Agendamentos {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String funcionario;
	public String getFuncionario() {
		return funcionario;
	}
	public void setFuncionario(String funcionario) {
		this.funcionario = funcionario;
	}
	private Date dtAgendamento;
	public Date getDtAgendamento() {
		return dtAgendamento;
	}
	public void setDtAgendamento(Date dtAgendamento) {
		this.dtAgendamento = dtAgendamento;
	}
	public String getServiços() {
		return serviços;
	}
	public void setServiços(String serviços) {
		this.serviços = serviços;
	}
	public Date getHora_agendamento() {
		return hora_agendamento;
	}
	public void setHora_agendamento(Date hora_agendamento) {
		this.hora_agendamento = hora_agendamento;
	}
	private String serviços;
	private Date hora_agendamento;
	public static String getId() {
		// TODO Auto-generated method stub
		return null;
	}

}
