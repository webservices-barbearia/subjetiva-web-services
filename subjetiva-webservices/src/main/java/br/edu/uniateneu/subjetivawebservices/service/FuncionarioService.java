package br.edu.uniateneu.subjetivawebservices.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.edu.uniateneu.subjetivawebservices.modelo.Funcionario;
import br.edu.uniateneu.subjetivawebservices.modelo.ResponseModel;
import br.edu.uniateneu.subjetivawebservices.repository.FuncionarioRepository;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.Produces;

@RestController
@RequestMapping(value = "/funcionario")
public class FuncionarioService {
	@Autowired
	FuncionarioRepository funcionarioRepository;

@Consumes("application/json")
@Produces("application/json")
@RequestMapping(value = "/salvar", method = RequestMethod.POST)
public @ResponseBody Funcionario salvar(@RequestBody Funcionario end) {
	Funcionario entity = this.funcionarioRepository.save(end);
	return entity;

}

@Produces("application/json")
@RequestMapping (value = "/list", method = RequestMethod.GET)
public @ResponseBody List<Funcionario> listar () {
	List<Funcionario> entities = this.funcionarioRepository.findAll ();
	return entities;

}

@Consumes("application/json")
@Produces("application/json")
@RequestMapping(value = "/atualizar", method = RequestMethod.PUT)
public @ResponseBody Funcionario atualizar (@RequestBody Funcionario end) {
	Funcionario entity = this.funcionarioRepository.save(end);
	return entity;

}

@Produces("application/json")
@RequestMapping (value = "/deletar/{codigo}", method = RequestMethod.DELETE)
public @ResponseBody ResponseModel excluir(@PathVariable("codigo") Long codigo)
{
	try {
	Funcionario funcionarios = funcionarioRepository.getReferenceById(codigo);
	System.out.println("********"+ Funcionario.getId());
		
	if (funcionarioRepository.findById(codigo).isEmpty()) {
				return new ResponseModel(500, "Funcionario não encontrado.");
	}	
				funcionarioRepository.delete(funcionarios) ;
				return new ResponseModel(200, "Registro excluido com sucesso!");

	} catch (Exception e) {
	return new ResponseModel(500, e.getMessage());
}
}}
