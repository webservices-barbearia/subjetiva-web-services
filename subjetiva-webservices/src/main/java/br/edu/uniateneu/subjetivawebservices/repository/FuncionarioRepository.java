package br.edu.uniateneu.subjetivawebservices.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.edu.uniateneu.subjetivawebservices.modelo.Funcionario;
@Repository
public interface FuncionarioRepository extends JpaRepository<Funcionario, Long>{

}
