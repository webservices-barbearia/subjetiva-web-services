package br.edu.uniateneu.subjetivawebservices.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.edu.uniateneu.subjetivawebservices.modelo.Agendamentos;
@Repository
public interface AgendamentosRepository extends JpaRepository<Agendamentos, Long>{

}
