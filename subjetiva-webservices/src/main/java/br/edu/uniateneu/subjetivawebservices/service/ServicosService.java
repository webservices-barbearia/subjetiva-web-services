package br.edu.uniateneu.subjetivawebservices.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.edu.uniateneu.subjetivawebservices.modelo.ResponseModel;
import br.edu.uniateneu.subjetivawebservices.modelo.Servicos;
import br.edu.uniateneu.subjetivawebservices.repository.ServicosRepository;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.Produces;

@RestController
@RequestMapping(value = "/servicos")
public class ServicosService {
	@Autowired
	ServicosRepository servicosRepository;

@Consumes("application/json")
@Produces("application/json")
@RequestMapping(value = "/salvar", method = RequestMethod.POST)
public @ResponseBody Servicos salvar(@RequestBody Servicos end) {
	Servicos entity = this.servicosRepository.save(end);
	return entity;

}

@Produces("application/json")
@RequestMapping (value = "/list", method = RequestMethod.GET)
public @ResponseBody List<Servicos> listar () {
	List<Servicos> entities = this.servicosRepository.findAll ();
	return entities;

}

@Consumes("application/json")
@Produces("application/json")
@RequestMapping(value = "/atualizar", method = RequestMethod.PUT)
public @ResponseBody Servicos atualizar (@RequestBody Servicos end) {
	Servicos entity = this.servicosRepository.save(end);
	return entity;

}

@Produces("application/json")
@RequestMapping (value = "/deletar/{codigo}", method = RequestMethod.DELETE)
public @ResponseBody ResponseModel excluir(@PathVariable("codigo") Long codigo)
{
	try {
	Servicos servico = servicosRepository.getReferenceById(codigo);
	System.out.println("*******"+ servico.getId());
		
	if (servicosRepository.findById(codigo).isEmpty()) {
				return new ResponseModel(500, "Serviço não encontrado.");
	}	
				servicosRepository.delete(servico) ;
				return new ResponseModel(200, "Registro excluido com sucesso!");

	} catch (Exception e) {
	return new ResponseModel(500, e.getMessage());
}
}}


